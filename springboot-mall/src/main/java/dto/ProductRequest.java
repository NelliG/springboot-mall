package dto;



import constant.ProductCategory;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ProductRequest {
	
	@NotNull
	private String pname;
//	@NotNull
	private ProductCategory cat;
//	@NotNull
	private String imgurl;
	@NotNull
	private int price;
	@NotNull
	private int stock;
	
	private String description;
	
}

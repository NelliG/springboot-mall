package com.example.demo.model;


import java.sql.Timestamp;

import constant.ProductCategory;
import lombok.Data;

@Data
public class Product {
	
	private int pid;
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	
	public String getImgurl() {
		return imgurl;
	}
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Timestamp getCreadate() {
		return creadate;
	}
	public void setCreadate(Timestamp creadate) {
		this.creadate = creadate;
	}
	public Timestamp getLastdate() {
		return lastdate;
	}
	public void setLastdate(Timestamp lastdate) {
		this.lastdate = lastdate;
	}
	private String pname;
	private ProductCategory cat;
	private String imgurl;
	private int price;
	private int stock;
	private String description;
	private Timestamp creadate;
	private Timestamp lastdate;

}

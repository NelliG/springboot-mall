package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.dao.ProdectDao;
import com.example.demo.model.Product;

import dto.ProductRequest;

@Component
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	private ProdectDao productDao;
	
	@Override
	public Product getProductById(int productId) {
		return productDao.getProductById(productId);
	}

	@Override
	public int createProduct(ProductRequest productRequest) {
		return productDao.createProduct(productRequest);
	}
}

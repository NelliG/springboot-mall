package com.example.demo.service;

import com.example.demo.model.Product;

import dto.ProductRequest;

public interface ProductService {
	
	Product getProductById(int productId);

	int createProduct(ProductRequest productRequest);
}

package com.example.demo.dao;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.example.demo.dao.ProdectDao;
import com.example.demo.model.Product;

import dto.ProductRequest;
import rowmapper.ProductRowMapper;

@Component
public class ProductDaolmpl implements ProdectDao{
	
	@Autowired
	private NamedParameterJdbcTemplate namePJT;
	
	
	@Override
	public Product getProductById(int productId) {
		String sql = "SELECT p_id,p_name, category, image_url, price, stock, description, create_date, last_modified_date FROM product WHERE p_id = :pid";
		Map<String, Object>map = new HashMap<>();
		map.put("pid", productId);
		
		List<Product>productList =namePJT.query(sql, map, new ProductRowMapper());
		if(productList.size()>0) {
			return productList.get(0);
		}
		else{
		return null;
		}
		}

	public int createProduct(ProductRequest productRequest) {
		String sql2 = "INSERT INTO product(p_name, category, image_url, price, stock, description, create_date, last_modified_date)VALUES(:pname,:cat,:img,:price,:stock,:des,:creadate,:lastdate)";
		Map<String, Object>map2 = new HashMap<>();
		map2.put("pname",productRequest.getPname());
		map2.put("cat", productRequest.getCat().name());
		map2.put("img", productRequest.getImgurl());
		map2.put("price", productRequest.getPrice());
		map2.put("stock", productRequest.getStock());
		map2.put("des", productRequest.getDescription());
		
		Date now = new Date();//記錄當下的時間
		map2.put("creadate", now);
		map2.put("lastdate", now);
		
		KeyHolder kh = new GeneratedKeyHolder();//儲存資料庫自動生成的productId
		namePJT.update(sql2, new MapSqlParameterSource(map2), kh);
		int productId = kh.getKey().intValue();
		
		return productId;//回傳
	}
}

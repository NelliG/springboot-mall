package com.example.demo.dao;

import com.example.demo.model.Product;

import dto.ProductRequest;

public interface ProdectDao {

	Product getProductById(int productId);
	
	int createProduct(ProductRequest productRequest);
}

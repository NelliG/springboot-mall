package constant;

//ENUM可以定義它裡面存放了哪一些固定值(須為大寫)
//1.如果為兩個單詞所組成需使用底線連接
public enum ProductCategory {
  
  FOOD,
  CAR,
  E_BOOK,
  
}

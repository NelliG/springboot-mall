package rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.demo.model.Product;

import constant.ProductCategory;

public class ProductRowMapper implements RowMapper<Product>{

	
	@Override
	public Product mapRow(ResultSet result, int i)throws SQLException{
		Product product = new Product();
		
		System.out.println("result = {}");
		product.setPid(result.getInt("p_id"));
		product.setPname(result.getString("p_name"));
		//將字串轉換成productcategory.寫法一
		String catStr = result.getString("category");
		ProductCategory catt = ProductCategory.valueOf(catStr);
		product.setCat(catt);
		
		//將字串轉換成productcategory.寫法二
		//product.setCat(ProductCategory.valueOf(result.getString("category")));
		
		product.setImgurl(result.getString("image_url"));
		product.setPrice(result.getInt("price"));
		product.setStock(result.getInt("stock"));
		product.setDescription(result.getString("description"));
		product.setCreadate(result.getTimestamp("create_date"));
		product.setLastdate(result.getTimestamp("last_modified_date"));
		
		return product;
	}
}
